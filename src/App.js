import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import Progressbar from './containers/Progressbar';

const QuestionPage = React.lazy(() => import('./containers/QuestionPage'));
const CreateVideoQn = React.lazy(() => import('./containers/CreateVideoQn'));
const ErrorPage = React.lazy(() => import('./containers/ErrorPage'));
const ThankyouPage = React.lazy(() => import('./containers/ThankyouPage'));
const YoutubePage = React.lazy(() => import('./containers/QuestionPage/youtube'));

const App = () => {
  const theme = {
    backgroundColor: '#F2F2F2',
    contentBackgroundColor: '#FFF',
    headerTextColor: '#FFF',
    bodyTextColor: '#59163B',
    themeColor: '#0033cc'
  };

  return (
    <BrowserRouter>
      <React.Suspense fallback={<Progressbar theme={theme} />}>
        <Switch>
          <Route exact path='/' render={(props) => <ErrorPage {...props} theme={theme} />} />
          <Route exact path='/event/:eventId' render={(props) => <QuestionPage {...props} theme={theme} />} />
          <Route
            exact
            path='/event/:eventId/collaborator/:collaboratorId'
            render={(props) => <QuestionPage {...props} theme={theme} />}
          />
          <Route exact path='/youtubepage' render={(props) => <YoutubePage {...props} theme={theme} />} />
          <Route exact path='/create/video/qn' render={(props) => <CreateVideoQn {...props} theme={theme} />} />
          <Route exact path='/error' render={(props) => <ErrorPage {...props} theme={theme} />} />
          <Route exact path='/thankyou/:eventId' render={(props) => <ThankyouPage {...props} theme={theme} />} />
        </Switch>
      </React.Suspense>
    </BrowserRouter>
  );
};

export default App;
