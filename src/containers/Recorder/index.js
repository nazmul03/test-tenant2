import React from 'react';
import axios from 'axios';
// import { isMobile } from 'react-device-detect';
import { makeStyles } from '@material-ui/core/styles';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import StopIcon from '@material-ui/icons/Stop';
import MicIcon from '@material-ui/icons/Mic';
import CloseIcon from '@material-ui/icons/Close';
import ReplayIcon from '@material-ui/icons/Replay';
import DoneIcon from '@material-ui/icons/Done';
import AudioRecorder from 'audio-recorder-polyfill';
import { useWindowSize } from 'react-use';

import { generateNameFromUrl } from '../utils';
import Progressbar from '../Progressbar';
import { formatTime } from '../utils';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '80vh',
    justifyContent: 'center',
    alignItems: 'center'
  },
  audioButton: {
    border: (theme) => `2px solid ${theme && theme.themeColor}`,
    backgroundColor: '#fff',
    color: (theme) => theme.themeColor,
    padding: '0',
    borderRadius: '50%',
    transition: 'all 0.3s ease-in',
    '&:hover': {
      boxShadow: 'none',
      color: '#FFF',
      backgroundColor: (theme) => theme.themeColor
    }
  }
}));

const Recorder = (props) => {
  const classes = useStyles(props && props.theme);
  const [capturing, setCapturing] = React.useState(false);
  const [recordedChunks, setRecordedChunks] = React.useState([]);
  const [blob, setBlob] = React.useState(null);
  const [url, setUrl] = React.useState(null);
  const [progressbar, setProgressbar] = React.useState(false);
  const [countdown, setCountdown] = React.useState(3);
  const [startCountdown, setStartCountdown] = React.useState(false);
  const [recordingLength, setRecordingLength] = React.useState({
    h: 0,
    m: 0,
    s: 0
  });
  const [maxVideoLength, setMaxVideoLength] = React.useState(null);
  const [tracks, setTracks] = React.useState(null);

  const mediaRecorderRef = React.useRef(null);
  const timeRef = React.useRef();
  const countdownRef = React.useRef();
  const maxVideoLengthRef = React.useRef(props && props.maxVideoLength);
  const audioRef = React.useRef();
  const { height } = useWindowSize();

  React.useEffect(() => {
    if (audioRef.current) {
      audioRef.current.style.height = height + 'px';
    }
  }, [height]);

  React.useEffect(() => {
    setMaxVideoLength(props && props.maxVideoLength);
  }, [props]);

  const timeCount = () => {
    setRecordingLength((prev) => {
      let secs = prev.s + 1;
      let hours = Math.floor(secs / (60 * 60));

      let divisor_for_minutes = secs % (60 * 60);
      let minutes = Math.floor(divisor_for_minutes / 60);

      let divisor_for_seconds = divisor_for_minutes % 60;
      let seconds = Math.ceil(divisor_for_seconds);
      return {
        h: hours,
        m: minutes,
        s: seconds
      };
    });
  };

  const startTimer = React.useCallback(() => {
    timeRef.current = setInterval(timeCount, 1000);
  }, []);

  const handleDataAvailable = ({ data }) => {
    if (data.size > 0) {
      setRecordedChunks((prev) => prev.concat(data));
    }
  };

  const startCountdownFn = () => {
    setStartCountdown(true);
    countdownRef.current = setInterval(() => {
      setCountdown((prevCountdown) => prevCountdown - 1);
    }, 1000);
  };

  const decreaseMaxVideoLength = () => {
    maxVideoLengthRef.current = setInterval(() => {
      setMaxVideoLength((prevVidLength) => prevVidLength - 1);
    }, 1000);
  };

  React.useEffect(() => {
    try {
      if (window.MediaRecorder) {
        navigator.mediaDevices
          .getUserMedia({ audio: true })
          .then((res) => {
            if (res.active === true) {
              const stream = res;
              setTracks(stream.getTracks());
              mediaRecorderRef.current = new MediaRecorder(stream);
              mediaRecorderRef.current.addEventListener('dataavailable', handleDataAvailable);
            } else {
              props.setMessage('Media device is not active');
            }
          })
          .catch((err) => {
            console.log(err);
            props.setMessage(`Error in User Media, Please check the permission again, ${err.toString()}`);
          });
      } else {
        let MediaRecorder = AudioRecorder;
        navigator.mediaDevices
          .getUserMedia({ audio: true })
          .then((res) => {
            if (res.active === true) {
              const stream = res;
              setTracks(stream.getTracks());
              mediaRecorderRef.current = new MediaRecorder(stream);
              mediaRecorderRef.current.addEventListener('dataavailable', handleDataAvailable);
            } else {
              props.setMessage('Media device is not active');
            }
          })
          .catch((err) => {
            props.setMessage(`Error in User Media, Please check the permission again, ${err.toString()}`);
          });
      }
    } catch (err) {
      props.setMessage(`Error in Media device, ${err.toString()}`);
    }

    // eslint-disable-next-line
  }, []);

  const startRecording = React.useCallback(() => {
    setStartCountdown(false);
    setCountdown(0);
    setCapturing(true);
    startTimer();
    decreaseMaxVideoLength();
    mediaRecorderRef.current.start(10);
  }, [startTimer]);

  React.useEffect(() => {
    if (countdown === 0) {
      clearInterval(countdownRef.current);
      startRecording();
    }
  }, [countdown, startRecording]);

  const stopRecording = () => {
    mediaRecorderRef.current.stop();
    clearInterval(timeRef.current);
    clearInterval(maxVideoLengthRef.current);
    setCapturing(false);
    const blob = new Blob(recordedChunks, {
      type: 'audio/mp3'
    });
    const url = URL.createObjectURL(blob);
    setBlob(blob);
    setUrl(url);
    setRecordingLength({
      h: 0,
      m: 0,
      s: 0
    });
  };

  const resetRecorder = () => {
    setRecordedChunks([]);
    setMaxVideoLength(props && props.maxVideoLength);
    setCountdown(3);
    setUrl(null);
    setBlob(null);
  };

  React.useEffect(() => {
    if (maxVideoLength === 0) {
      stopRecording();
    }
    // eslint-disable-next-line
  }, [maxVideoLength]);

  const submitAudio = () => {
    if (recordedChunks.length) {
      blob.name = generateNameFromUrl('Recorder', 'mp3');
      blob.url = url;

      console.log(blob);

      let formData = new FormData();
      formData.append('file', blob, blob.name);

      setProgressbar(true);
      axios
        .post('https://api.miljulapp.com/api/v1/common/fileUpload', formData)
        .then((response) => {
          console.log(response);
          if (response.status === 200) {
            props.setMediaUrl(response.data.url);
            setProgressbar(false);
            if (props.collaboratorId) {
              props.createColabMedia(response.data.url, props.eventId, props.collaboratorId);
            } else props.setOpenForm(true);
          } else {
            setProgressbar(false);
            props.setMessage('Error in File Upload Process');
          }
        })
        .catch((error) => {
          console.log(error);
          setProgressbar(false);
          props.setMessage('Error in File Upload Process');
        });

      window.URL.revokeObjectURL(url);
      setRecordedChunks([]);
    }
  };

  const stopTracks = () => {
    tracks &&
      tracks.forEach(function (track) {
        track.stop();
      });
    return;
  };

  // console.log(props);

  return (
    <div ref={audioRef}>
      {audioRef && audioRef.current ? (
        <>
          {url ? (
            <div className={classes.root}>
              <audio autoPlay={true} controls={true} src={url}></audio>
              <div style={{ position: 'absolute', bottom: '15vh', left: '0', width: '100%', textAlign: 'center' }}>
                <button className={classes.audioButton} style={{ marginRight: '10px' }} onClick={resetRecorder}>
                  <ReplayIcon
                    style={{
                      fontSize: '60px'
                    }}
                  />
                </button>
                <button className={classes.audioButton} style={{ marginLeft: '10px' }} onClick={submitAudio}>
                  <DoneIcon
                    style={{
                      fontSize: '60px'
                    }}
                  />
                </button>
              </div>
              {progressbar ? <Progressbar theme={props && props.theme} /> : null}
            </div>
          ) : (
            <>
              <div className={classes.root}>
                <div>
                  <MicIcon style={{ fontSize: '200px', color: props && props.theme && props.theme.themeColor }} />
                </div>
                <div style={{ position: 'absolute', top: 0, right: 0 }}>
                  <button
                    style={{
                      border: `2px solid ${props && props.theme && props.theme.themeColor}`,
                      backgroundColor: props && props.theme && props.theme.themeColor,
                      color: '#fff',
                      padding: '5px 10px',
                      borderBottomLeftRadius: '5px'
                    }}
                    onClick={() => {
                      stopTracks();
                      props.setOpenAudioRecorder(false);
                      props.setLeftVideo(true);
                      props.setOptions(true);
                    }}
                  >
                    <CloseIcon />
                  </button>
                </div>
                <div style={{ position: 'absolute', bottom: '15vh', left: '0', width: '100%', textAlign: 'center' }}>
                  {maxVideoLength ? (
                    maxVideoLength === 0 ? (
                      <span>00: 00</span>
                    ) : (
                      <div
                        style={{
                          fontSize: '2rem',
                          textAlign: 'center',
                          color: props && props.theme && props.theme.themeColor
                        }}
                      >
                        <span>{formatTime(maxVideoLength)}</span>
                      </div>
                    )
                  ) : (
                    <div
                      style={{
                        fontSize: '2rem',
                        textAlign: 'center',
                        color: props && props.theme && props.theme.themeColor
                      }}
                    >
                      <span>
                        {recordingLength.m !== undefined
                          ? `${recordingLength.m <= 9 ? '0' + recordingLength.m : recordingLength.m}`
                          : '00'}
                      </span>
                      <span>:</span>
                      <span>
                        {recordingLength.s !== undefined
                          ? `${recordingLength.s <= 9 ? '0' + recordingLength.s : recordingLength.s}`
                          : '00'}
                      </span>
                    </div>
                  )}

                  {capturing ? (
                    <button className={classes.audioButton} onClick={stopRecording}>
                      <StopIcon
                        style={{
                          fontSize: '60px'
                        }}
                      />
                    </button>
                  ) : (
                    <button className={classes.audioButton} onClick={startCountdownFn}>
                      <FiberManualRecordIcon
                        style={{
                          fontSize: '60px'
                        }}
                      />
                    </button>
                  )}
                </div>

                {/************** Countdown *****************/}
                {startCountdown ? (
                  <div style={{ position: 'absolute', top: '0', left: '0', width: '100%', height: '100%' }}>
                    <div
                      style={{
                        display: 'flex',
                        height: '100%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor: 'rgba(0, 0, 0, 0.6)'
                      }}
                    >
                      <div style={{ fontSize: '8rem', color: props && props.theme && props.theme.themeColor }}>
                        {countdown === 0 ? 'GO' : countdown}
                      </div>
                    </div>
                  </div>
                ) : null}
              </div>
            </>
          )}
        </>
      ) : (
        <Progressbar theme={props.theme} />
      )}
    </div>
  );
};

export default Recorder;
