import React from 'react';
import { withRouter } from 'react-router';
import { isMobile } from 'react-device-detect';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import axios from 'axios';

import Progressbar from '../Progressbar';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    textAlign: 'center',
    padding: isMobile ? '1rem 2rem' : '1rem 0',
    '& .MuiTextField-root': {
      margin: '6px 0',
      width: isMobile ? '100%' : '90%',
      borderRadius: '4px',
      '& .MuiOutlinedInput-root': {
        color: (theme) => theme.themeColor,
        '& .MuiOutlinedInput-notchedOutline': {
          border: (theme) => `1px solid ${theme && theme.themeColor}`
        }
      },
      '& .MuiFormLabel-root': {
        color: '#FFF'
      }
    },
    '& .Mui-focused': {
      color: (theme) => theme.themeColor,
      '& .MuiOutlinedInput-notchedOutline': {
        border: (theme) => `1px solid ${theme && theme.themeColor}`
      }
    }
  },
  fileButton: {
    border: (theme) => `2px solid ${theme && theme.themeColor}`,
    backgroundColor: (theme) => theme.themeColor,
    color: '#fff',
    fontSize: '18px',
    fontWeight: '400',
    padding: '5px 20px',
    borderRadius: '4px',
    transition: 'all 0.3s ease-in',
    '&:hover': {
      boxShadow: 'none',
      color: '#fff',
      backgroundColor: (theme) => theme.themeColor
    }
  }
}));

const Form = (props) => {
  const classes = useStyles(props && props.theme);
  const [user, setUser] = React.useState({
    name: '',
    email: '',
    mobile: '',
    meta_data: {
      social_media: ''
    }
  });
  const [progressbar, setProgressbar] = React.useState(false);

  const handleInputChange = (e) => {
    setUser({
      ...user,
      [e.target.id]: e.target.value
    });
  };

  const handleSocialMediaChange = (e) => {
    setUser({
      ...user,
      meta_data: {
        [e.target.id]: e.target.value
      }
    });
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    if (props.mediaUrl) {
      if (user.name === '' || user.email === '') {
        props.setMessage('Please Fillup all the required fields');
      } else {
        setProgressbar(true);
        const collaborator = { ...user, collab_event_id: props.eventId, collborator_type: 'OTHERS' };
        axios
          .post('https://api.miljulapp.com/api/v1/api/v1/collab/collaborators', collaborator)
          .then((response) => {
            console.log(response.data.response.response);
            props.createColabMedia(
              props.mediaUrl,
              response.data.response.response.collab_event_id,
              response.data.response.response.id
            );
          })
          .catch((error) => {
            console.log(error);
            setProgressbar(false);
            props.setMessage('Collaborator could not be created');
          });
      }
    } else {
      props.setMessage('Please upload a media first');
    }
  };
  console.log(props);

  return (
    <div
      style={{
        height: '100%',
        width: isMobile ? '85vw' : '30vw',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <div style={{ display: 'flex', justifyContent: 'flex-end', width: '100%' }}>
        <button
          style={{
            border: `2px solid ${props && props.theme && props.theme.themeColor}`,
            backgroundColor: props && props.theme && props.theme.themeColor,
            color: '#fff',
            padding: '2px 5px',
            borderBottomLeftRadius: '4px'
          }}
          onClick={() => props.setOpenForm(false)}
        >
          <CloseIcon />
        </button>
      </div>
      <div style={{ fontSize: '24px', fontWeight: '500', color: 'rgba(255, 255, 255, 0.7)' }}>
        Please provide your info
      </div>
      <form className={classes.root} noValidate autoComplete='off' onSubmit={handleFormSubmit}>
        <TextField
          id='name'
          label={
            <span>
              Name <span style={{ color: '#C72515' }}>*</span>
            </span>
          }
          type='text'
          value={user.name}
          variant='outlined'
          placeholder='John Doe'
          onChange={handleInputChange}
        />
        <TextField
          id='email'
          label={
            <span>
              Email <span style={{ color: '#C72515' }}>*</span>
            </span>
          }
          type='email'
          value={user.email}
          variant='outlined'
          placeholder='johndoe@email.com'
          onChange={handleInputChange}
        />
        <TextField
          id='mobile'
          label='Phone'
          type='text'
          value={user.mobile}
          variant='outlined'
          placeholder='0000011111'
          onChange={handleInputChange}
        />
        <TextField
          id='social_media'
          label='Social Handle'
          type='text'
          value={user.meta_data.social_media}
          variant='outlined'
          placeholder='@johnDoe'
          onChange={handleSocialMediaChange}
        />
        <div style={{ width: 'fit-content', margin: 'auto' }}>
          <button type='submit' className={classes.fileButton}>
            <span>Submit</span>
          </button>
        </div>
      </form>
      {progressbar ? <Progressbar theme={props && props.theme} /> : null}
    </div>
  );
};

export default withRouter(Form);
