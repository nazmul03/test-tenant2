import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  paper: {
    backgroundColor: (props) => props.backgroundColor || theme.palette.background.paper,
    boxShadow: (props) =>
      props.borderColor
        ? `0px 3px 5px -1px ${props.borderColor}, 0px 5px 8px 0px ${props.borderColor}, 0px 1px 14px 0px ${props.borderColor}`
        : theme.shadows[5],
    padding: (props) => props.padding || theme.spacing(2, 4, 2),
    width: 'fit-content',
    height: 'fit-content',
    maxWidth: '42rem',
    borderRadius: '10px',
    fontSize: '18px',
    '&:focus, &:focus-visible': {
      outline: 'none'
    }
  }
}));

function CustomModal(props) {
  const classes = useStyles({
    backgroundColor: props && props.backgroundColor,
    padding: props && props.padding,
    borderColor: props && props.borderColor
  });

  return (
    <Modal
      aria-labelledby='transition-modal-title'
      aria-describedby='transition-modal-description'
      className={classes.modal}
      open={props.open ? true : false}
      onClose={props.onClose || null}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500
      }}
    >
      <Fade in={props.open ? true : false}>
        <div className={classes.paper}>{props.children}</div>
      </Fade>
    </Modal>
  );
}

export default CustomModal;
