import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ReactPlayer from 'react-player';

const useStyles = makeStyles((theme) => ({
  reactPlayer: {
    '& video': {
      objectFit: 'cover'
    }
  }
}));

const VideoPlayer = (props) => {
  const classes = useStyles(props && props.theme);

  console.log(props);

  return (
    <>
      <ReactPlayer
        url={props.url}
        height='100vh'
        width='100%'
        controls={true}
        playsinline={true}
        muted={true}
        playing={true}
        className={classes.reactPlayer}
      />
    </>
  );
};

export default VideoPlayer;
