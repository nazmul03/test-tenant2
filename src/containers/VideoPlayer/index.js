import React from 'react';
import ReactPlayer from 'react-player';
import { makeStyles } from '@material-ui/core/styles';
import PlayArrowRoundedIcon from '@material-ui/icons/PlayArrowRounded';
// import LinearProgress from '@material-ui/core/LinearProgress';

// import Progressbar from '../Progressbar';
import { formatTime } from '../utils';

const useStyles = makeStyles((theme) => ({
  reactPlayer: {
    '& video': {
      objectFit: 'cover'
    }
  },
  playButton: {
    border: (theme) => `2px solid ${theme && theme.themeColor}`,
    backgroundColor: '#fff',
    color: (theme) => theme.themeColor,
    padding: '0',
    borderRadius: '50%',
    transition: 'all 0.3s ease-in',
    '&:hover': {
      boxShadow: 'none',
      color: '#FFF',
      backgroundColor: (theme) => theme.themeColor
    }
  }
}));

const VideoPlayer = (props) => {
  const classes = useStyles(props && props.theme);

  // const [controlVisible, setControlVisible] = React.useState(false);
  const [playButton, setPlayButton] = React.useState(true);
  const [playing, setPlaying] = React.useState(true);
  const [mute, setMute] = React.useState(true);
  const [playedTime, setPlayedTime] = React.useState(0);
  // const [playedPercent, setPlayedPercent] = React.useState(0);
  const [duration, setDuration] = React.useState(0);

  const videoRef = React.useRef();

  const handlePlayButton = () => {
    setPlayButton(false);
    setMute(false);
    setPlaying(true);
  };

  const playButtonDesign = (
    <div
      style={{
        position: 'absolute',
        top: '0',
        left: '0',
        width: '100%',
        height: '100%'
      }}
    >
      <div
        style={{
          display: 'flex',
          width: '100%',
          height: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'rgba(0, 0, 0, 0.5)'
        }}
        onClick={handlePlayButton}
      >
        <button className={classes.playButton}>
          <PlayArrowRoundedIcon style={{ fontSize: '80px' }} />
        </button>
      </div>
    </div>
  );

  return (
    <>
      <ReactPlayer
        ref={videoRef}
        id='video'
        url={props.url}
        height='100vh'
        width='100%'
        playsinline
        // controls
        // loop={playButton}
        playing={playing}
        muted={mute}
        // playbackRate={0.5}
        onPlay={() => {
          setPlaying(true);
        }}
        onPause={() => {
          setPlaying(false);
        }}
        onProgress={(data) => {
          // console.log(data);
          setPlayedTime(data.playedSeconds);
          // setPlayedPercent(data.played);
          setDuration(Math.round(videoRef && videoRef.current && videoRef.current.getDuration()));
        }}
        onClick={() => setPlaying(false)}
        className={classes.reactPlayer}
      />

      <div
        style={{
          position: 'absolute',
          top: '0',
          left: '0',
          width: '100%',
          zIndex: '9'
        }}
      >
        {duration > 0 && isFinite(duration) ? (
          <>
            {/* <LinearProgress variant='determinate' value={playedPercent * 100} /> */}
            <div
              style={{ display: 'flex', width: '100%', justifyContent: 'flex-end', color: '#fff', padding: '5px 10px' }}
            >
              {`${formatTime(playedTime)}/${formatTime(duration)}`}
            </div>
          </>
        ) : null}
      </div>
      {!playing ? playButtonDesign : null}
      {playButton ? playButtonDesign : null}
    </>
  );
};

export default VideoPlayer;
