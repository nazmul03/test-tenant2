import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';

import happy from '../../assets/images/happy.png';

const Thankyou = (props) => {
  const [thankyouMessage, setThankyouMessage] = React.useState(null);
  const defaultThankyouMessage = 'Thank you for participating';

  const getMessageFromApi = (colabEventId) => {
    axios
      .get(`https://api.miljulapp.com/api/v1/api/v1/collab/collabevent/${colabEventId}`)
      .then((response) => {
        console.log(response);
        if (response.data.response.response.meta_data) {
          if (response.data.response.response.meta_data.ty_callout) {
            setThankyouMessage(response.data.response.response.meta_data.ty_callout);
          } else {
            setThankyouMessage(defaultThankyouMessage);
          }
        } else {
          setThankyouMessage(defaultThankyouMessage);
        }
      })
      .catch((error) => {
        console.log(error);
        setThankyouMessage(defaultThankyouMessage);
      });
  };

  React.useEffect(() => {
    props && props.location.state
      ? props.location.state.message
        ? setThankyouMessage(props.location.state.message)
        : getMessageFromApi(props.match.params.eventId)
      : getMessageFromApi(props.match.params.eventId);
  }, [props]);

  return (
    <div
      style={{
        backgroundColor: 'inherit',
        width: '100vw',
        height: '70vh',
        position: 'relative',
        margin: '15px 0',
        overflow: 'hidden'
      }}
    >
      <div className='row m-0 p-0' style={{ width: '100%', position: 'absolute', top: '20%' }}>
        {thankyouMessage && (
          <div style={{ textAlign: 'center', width: 'fit-content', margin: '0 auto', padding: '1rem' }}>
            <div style={{ margin: '1rem 0' }}>
              <img src={happy} alt='Happy' style={{ maxWidth: '90vw' }} />
            </div>
            <div
              style={{ fontSize: '3vmax', fontWeight: '500', color: props && props.theme && props.theme.themeColor }}
            >
              {thankyouMessage}
            </div>
            {/* <div style={{ margin: '1rem' }}>
              <Box
                says='Go back to form'
                does={() =>
                  props.history.push(
                    `/event/${
                      props && props.match
                        ? props.match
                          ? props.match.params
                            ? props.match.params
                              ? props.match.params.eventId
                              : '/'
                            : '/'
                          : '/'
                        : '/'
                    }`
                  )
                }
                theme={props.theme}
              />
            </div> */}
          </div>
        )}
      </div>
    </div>
  );
};

export default withRouter(Thankyou);
