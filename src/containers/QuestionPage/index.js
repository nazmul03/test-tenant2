import React from 'react';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import { isMobile } from 'react-device-detect';
import { makeStyles } from '@material-ui/core/styles';

import CustomModal from '../CustomModal';
import Progressbar from '../Progressbar';
const LeftScreen = React.lazy(() => import('./LeftScreen'));
const RightScreen = React.lazy(() => import('./RightScreen'));

const useStyles = makeStyles((theme) => ({
  button: {
    border: (theme) => `2px solid ${theme && theme.themeColor}`,
    backgroundColor: (theme) => theme.themeColor,
    color: '#fff',
    borderRadius: '10px',
    fontSize: '12px',
    fontWeight: '600',
    padding: '6px 10px',
    textAlign: 'center',
    textTransform: 'uppercase',
    boxShadow: theme.shadows[8],
    transition: 'all 0.3s ease-in',
    '&:hover': {
      boxShadow: 'none',
      color: (theme) => theme.themeColor,
      backgroundColor: '#fff'
    }
  },
  videoButton: {
    border: (theme) => `2px solid ${theme && theme.themeColor}`,
    backgroundColor: (theme) => theme.themeColor,
    color: '#fff',
    borderRadius: '10px',
    fontSize: '16px',
    fontWeight: '600',
    padding: '10px 20px',
    textAlign: 'center',
    textTransform: 'uppercase',
    boxShadow: theme.shadows[8],
    transition: 'all 0.3s ease-in',
    '&:hover': {
      boxShadow: 'none',
      color: (theme) => theme.themeColor,
      backgroundColor: '#fff'
    }
  },
  fileButton: {
    border: (theme) => `2px solid ${theme && theme.themeColor}`,
    backgroundColor: '#fff',
    color: (theme) => theme.themeColor,
    padding: '0',
    borderRadius: '50%',
    transition: 'all 0.3s ease-in',
    '&:hover': {
      boxShadow: 'none',
      color: '#FFF',
      backgroundColor: (theme) => theme.themeColor
    }
  }
}));

const QuestionPage = (props) => {
  const classes = useStyles(props && props.theme);
  const [eventData, setEventData] = React.useState(null);
  const [leftVideo, setLeftVideo] = React.useState(true);
  const [message, setMessage] = React.useState(null);
  const [eventId, setEventId] = React.useState(null);
  const [maxVideoLength, setMaxVideoLength] = React.useState(null);

  React.useEffect(() => {
    setEventId(props && props.match && props.match.params && props.match.params.eventId);
    axios
      .get(
        `https://api.miljulapp.com/api/v1/api/v1/collab/collabevent/${
          props && props.match && props.match.params && props.match.params.eventId
        }`
      )
      .then((response) => {
        console.log(response);
        setEventData(response.data.response.response);
        setMaxVideoLength(
          response.data.response.response.meta_data
            ? response.data.response.response.meta_data.max_video_length || 120
            : 120
        );
      })
      .catch((error) => {
        console.log(error);
        setMessage('Error in getting event');
      });
  }, [props]);

  console.log(props, message);

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <div
        className={isMobile ? null : 'row'}
        style={{
          width: '100%',
          boxShadow: '2px 2px 10px 10px #eee',
          backgroundColor: '#FFF',
          overflow: 'hidden',
          position: 'relative'
        }}
      >
        <div className={isMobile ? null : 'col-12 col-sm-6 p-0 m-0'} style={{ backgroundColor: '#F2F2F2' }}>
          <React.Suspense fallback={<Progressbar theme={props.theme} />}>
            {leftVideo ? <LeftScreen eventData={eventData} classes={classes} theme={props.theme} /> : null}
          </React.Suspense>
        </div>
        <div className={isMobile ? null : 'col-12 col-sm-6 p-0 m-0'}>
          <React.Suspense fallback={<Progressbar theme={props.theme} />}>
            <RightScreen
              eventId={eventId}
              setMessage={setMessage}
              setLeftVideo={setLeftVideo}
              maxVideoLength={maxVideoLength}
              classes={classes}
              theme={props.theme}
              collaboratorId={props && props.match && props.match.params && props.match.params.collaboratorId}
            />
          </React.Suspense>
        </div>
      </div>
      <CustomModal open={message} onClose={() => setMessage(null)} theme={props && props.theme}>
        <div style={{ paddingBottom: '20px', fontSize: '24px', textAlign: 'center' }}>{message}</div>
        <div style={{ textAlign: 'center' }}>
          <button className={classes.button} onClick={() => setMessage(null)}>
            <span style={{ padding: '6px 24px' }}>OK</span>
          </button>
        </div>
      </CustomModal>
    </div>
  );
};

export default withRouter(QuestionPage);
