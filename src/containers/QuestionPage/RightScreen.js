import React from 'react';
import axios from 'axios';
import { withRouter } from 'react-router';
import VideocamIcon from '@material-ui/icons/Videocam';
import MicIcon from '@material-ui/icons/Mic';
import PermMediaIcon from '@material-ui/icons/PermMedia';
import CloseIcon from '@material-ui/icons/Close';
import DoneIcon from '@material-ui/icons/Done';
import { isIOS, isMobile, isSafari } from 'react-device-detect';

import Form from '../SubmitForm';
import Progressbar from '../Progressbar';
import CustomModal from '../CustomModal';

const Camera = React.lazy(() => import('../Camera'));
const Recorder = React.lazy(() => import('../Recorder'));

const RightScreen = (props) => {
  const [mediaUrl, setMediaUrl] = React.useState(null);
  const [selectedFile, setSelectedFile] = React.useState(null);
  const [options, setOptions] = React.useState(true);
  const [openVideoCamera, setOpenVideoCamera] = React.useState(false);
  const [openAudioRecorder, setOpenAudioRecorder] = React.useState(false);
  const [openForm, setOpenForm] = React.useState(false);
  const [progressbar, setProgressbar] = React.useState(false);

  const handleFileSelect = (e) => {
    console.log(e.target.files);
    setSelectedFile(e.target.files[0]);
  };

  const handleVideoClick = () => {
    setSelectedFile(null);
    setOptions(false);
    if (isMobile) {
      props.setLeftVideo(false);
    }
    setOpenVideoCamera(true);
  };

  const handleAudioClick = () => {
    setSelectedFile(null);
    setOptions(false);
    if (isMobile) {
      props.setLeftVideo(false);
    }
    setOpenAudioRecorder(true);
  };

  const submitFile = (file) => {
    let formData = new FormData();
    formData.append('file', file, file.name);

    setProgressbar(true);
    axios
      .post('https://api.miljulapp.com/api/v1/common/fileUpload', formData)
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          setMediaUrl(response.data.url);
          setProgressbar(false);
          if (props.collaboratorId) {
            createColabMedia(response.data.url, props.eventId, props.collaboratorId);
          } else setOpenForm(true);
        } else {
          setProgressbar(false);
          console.log('Error in File Upload Process');
          props.setMessage('Error in File Upload Process');
        }
      })
      .catch((error) => {
        console.log(error);
        setProgressbar(false);
        props.setMessage('Error in File Upload Process');
      });
  };

  const createColabMedia = (mediaUrl, eventId, collaboratorId) => {
    // eslint-disable-next-line
    axios
      .post('https://api.miljulapp.com/api/v1/api/v1/collab/collabmedia', {
        media_url: mediaUrl,
        collab_event: eventId,
        event_collaborator: collaboratorId
      })
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          setProgressbar(false);
          props.history.push(`/thankyou/${props.eventId}`);
        } else {
          setProgressbar(false);
          props.setMessage('Media could not be created');
        }
      })
      .catch((error) => {
        console.log(error);
        setProgressbar(false);
        props.setMessage('Media could not be created');
      });
  };

  console.log(props);

  return (
    <>
      {options ? (
        <div
          style={
            isMobile
              ? {
                  position: 'absolute',
                  bottom: '10%',
                  left: '0',
                  width: '100%'
                }
              : {
                  display: 'flex',
                  flexDirection: 'column',
                  height: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  textAlign: 'center'
                }
          }
        >
          <div style={{ width: 'max-content', margin: 'auto' }}>
            <div
              style={{
                fontSize: '20px',
                fontWeight: '700',
                padding: '8px 0',
                color: isMobile ? '#FFF' : props && props.theme && props.theme.bodyTextColor
              }}
            >
              Choose any of these to answer
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '18rem' }}>
              {isMobile && isIOS && !isSafari ? (
                <label className={props.classes.button}>
                  <div>
                    <MicIcon style={{ fontSize: '30px', margin: '4px' }} />
                  </div>
                  <span style={{ padding: '0px 0px' }}>Audio</span>
                  <input
                    style={{ display: 'none' }}
                    type='file'
                    name='file'
                    id='file'
                    accept='image/*,audio/*,video/*'
                    onChange={(e) => submitFile(e.target.files[0])}
                  />
                </label>
              ) : (
                <button className={props.classes.button} onClick={handleAudioClick}>
                  <div>
                    <MicIcon style={{ fontSize: '30px', margin: '4px' }} />
                  </div>
                  <span style={{ padding: '0px 8px' }}>Audio</span>
                </button>
              )}

              {isMobile && isIOS && !isSafari ? (
                <label className={props.classes.videoButton}>
                  <div>
                    <VideocamIcon style={{ fontSize: '50px', margin: '4px' }} />
                  </div>
                  <span style={{ padding: '0px 8px' }}>Video</span>
                  <input
                    style={{ display: 'none' }}
                    type='file'
                    name='video'
                    id='video'
                    accept='image/*,audio/*,video/*'
                    onChange={(e) => submitFile(e.target.files[0])}
                  />
                </label>
              ) : (
                <button className={props.classes.videoButton} onClick={handleVideoClick}>
                  <div>
                    <VideocamIcon style={{ fontSize: '50px', margin: '4px' }} />
                  </div>
                  <span style={{ padding: '0px 8px' }}>Video</span>
                </button>
              )}

              <label className={props.classes.button}>
                <div>
                  <PermMediaIcon style={{ fontSize: '30px', margin: '4px' }} />
                </div>
                <span style={{ padding: '0px 0px' }}>Gallery</span>
                <input
                  style={{ display: 'none' }}
                  type='file'
                  name='file'
                  id='file'
                  accept='image/*,audio/*,video/*'
                  onChange={handleFileSelect}
                />
              </label>
            </div>
            <div
              style={{
                fontSize: '12px',
                fontWeight: '400',
                paddingTop: '20px',
                textAlign: 'center',
                maxWidth: '18rem'
              }}
            >
              {selectedFile ? (
                <>
                  <div style={{ color: isMobile ? '#FFF' : props.theme.themeColor }}>
                    <span>You have selected: </span>
                    <span style={{ fontSize: '14px', fontWeight: '500', fontStyle: 'italic' }}>
                      {selectedFile.name}
                    </span>
                  </div>
                  <div style={{ marginTop: '10px', width: '100%' }}>
                    <button
                      className={props.classes.fileButton}
                      style={{ marginRight: '10px' }}
                      onClick={() => {
                        setSelectedFile(null);
                      }}
                    >
                      <CloseIcon />
                    </button>
                    <button
                      className={props.classes.fileButton}
                      style={{ marginLeft: '10px' }}
                      onClick={() => submitFile(selectedFile)}
                    >
                      <DoneIcon />
                    </button>
                  </div>
                </>
              ) : null}
            </div>
          </div>
          {progressbar ? <Progressbar theme={props && props.theme} /> : null}
        </div>
      ) : null}
      {openVideoCamera ? (
        <React.Suspense fallback={<Progressbar theme={props && props.theme} />}>
          <Camera
            setOptions={setOptions}
            setOpenForm={setOpenForm}
            setOpenVideoCamera={setOpenVideoCamera}
            setLeftVideo={props.setLeftVideo}
            setMediaUrl={setMediaUrl}
            maxVideoLength={props.maxVideoLength}
            setMessage={props.setMessage}
            theme={props && props.theme}
            eventId={props.eventId}
            collaboratorId={props.collaboratorId}
            createColabMedia={createColabMedia}
          />
        </React.Suspense>
      ) : null}
      {openAudioRecorder ? (
        <React.Suspense fallback={<Progressbar theme={props && props.theme} />}>
          <Recorder
            setOptions={setOptions}
            setOpenForm={setOpenForm}
            setOpenAudioRecorder={setOpenAudioRecorder}
            setLeftVideo={props.setLeftVideo}
            setMediaUrl={setMediaUrl}
            maxVideoLength={props.maxVideoLength}
            setMessage={props.setMessage}
            theme={props && props.theme}
            eventId={props.eventId}
            collaboratorId={props.collaboratorId}
            createColabMedia={createColabMedia}
          />
        </React.Suspense>
      ) : null}
      {openForm ? (
        <CustomModal
          open={openForm}
          padding='0'
          backgroundColor='rgba(0, 0, 0, 0.4)'
          borderColor='rgba(166, 46, 102, 0.5)'
        >
          <Form
            setPreviousScreen={setOptions}
            setOpenForm={setOpenForm}
            setLeftVideo={props.setLeftVideo}
            mediaUrl={mediaUrl}
            eventId={props.eventId}
            setMessage={props.setMessage}
            theme={props && props.theme}
            createColabMedia={createColabMedia}
          />
        </CustomModal>
      ) : null}
    </>
  );
};

export default withRouter(RightScreen);
