import React from 'react';
// import { useWindowSize } from 'react-use';

import VideoPlayer from '../VideoPlayer';

const LeftScreen = (props) => {
  // const appRef = React.useRef();
  // const { height } = useWindowSize();

  // React.useEffect(() => {
  //   if (appRef.current) {
  //     appRef.current.style.height = height + 'px';
  //   }
  // }, [height]);

  // console.log(appRef);

  return (
    // <div ref={appRef}>
    <div>
      <>
        <VideoPlayer
          url={props.eventData && props.eventData.meta_data && props.eventData.meta_data.question}
          theme={props.theme}
        />
        <div
          style={{
            position: 'absolute',
            top: '0',
            left: '0',
            width: '100%',
            padding: '0 1rem'
          }}
        >
          <div
            style={{
              width: 'max-content',
              maxWidth: '100%',
              margin: '20px auto 0',
              fontSize: '4.5vmax',
              fontWeight: '700',
              wordWrap: 'normal',
              textAlign: 'center',
              color: props && props.theme && props.theme.headerTextColor
            }}
          >
            {(props.eventData && props.eventData.name) || ''}
          </div>
        </div>
      </>
    </div>
  );
};

export default LeftScreen;
