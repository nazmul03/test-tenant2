import React from 'react';

import success from '../../assets/images/success.png';

const ErrorPage = (props) => {
  return (
    <div
      style={{
        backgroundColor: props.theme ? props.theme.backgroundColor || 'inherit' : 'inherit',
        width: '100vw',
        height: '70vh',
        position: 'relative',
        margin: '15px 0',
        overflow: 'hidden'
      }}
    >
      <div className='row m-0 p-0' style={{ width: '100%', position: 'absolute', top: '20%' }}>
        <div style={{ width: 'fit-content', margin: 'auto', padding: '1rem' }}>
          <img src={success} alt='Success' style={{ maxWidth: '90vw' }} />
        </div>
      </div>
    </div>
  );
};

export default ErrorPage;
