export const generateNameFromUrl = (text, extension) => {
  var d = new Date();
  return `${text}-${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}-${d.getDate()}:${d.getMonth()}:${d.getFullYear()}.${extension}`;
};

export const formatTime = (time) => {
  return `${Math.floor(time / 60) <= 9 ? '0' + Math.floor(time / 60) : Math.floor(time / 60)}: ${
    Math.floor(time % 60) <= 9 ? '0' + Math.floor(time % 60) : Math.floor(time % 60)
  }`;
};
