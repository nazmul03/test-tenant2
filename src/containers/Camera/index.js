import React from 'react';
import axios from 'axios';
import CloseIcon from '@material-ui/icons/Close';
import ReplayIcon from '@material-ui/icons/Replay';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import StopIcon from '@material-ui/icons/Stop';
import DoneIcon from '@material-ui/icons/Done';
import { makeStyles } from '@material-ui/core/styles';
import AudioRecorder from 'audio-recorder-polyfill';
import { useWindowSize } from 'react-use';

import { generateNameFromUrl } from '../utils';
import Progressbar from '../Progressbar';
import { formatTime } from '../utils';

const useStyles = makeStyles((theme) => ({
  videoButtons: {
    border: (theme) => `2px solid ${theme && theme.themeColor}`,
    backgroundColor: '#fff',
    color: (theme) => theme.themeColor,
    padding: '0',
    borderRadius: '50%',
    transition: 'all 0.3s ease-in',
    '&:hover': {
      boxShadow: 'none',
      color: '#FFF',
      backgroundColor: (theme) => theme.themeColor
    }
  },
  reactPlayer: {
    '& video': {
      objectFit: 'cover'
    }
  }
}));

const Camera = (props) => {
  const classes = useStyles(props && props.theme);
  const [capturing, setCapturing] = React.useState(false);
  const [recordingScreen, setRecordingScreen] = React.useState(true);
  const [recordedChunks, setRecordedChunks] = React.useState([]);
  const [blob, setBlob] = React.useState(null);
  const [url, setUrl] = React.useState(null);
  const [progressbar, setProgressbar] = React.useState(false);
  const [countdown, setCountdown] = React.useState(3);
  const [startCountdown, setStartCountdown] = React.useState(false);
  const [recordingLength, setRecordingLength] = React.useState({
    h: 0,
    m: 0,
    s: 0
  });
  const [maxVideoLength, setMaxVideoLength] = React.useState(null);
  const [tracks, setTracks] = React.useState(null);

  const webcamRef = React.useRef();
  const mediaRecorderRef = React.useRef();
  const timeRef = React.useRef();
  const countdownRef = React.useRef();
  const maxVideoLengthRef = React.useRef(props && props.maxVideoLength);
  const cameraRef = React.useRef();
  const { height } = useWindowSize();

  React.useEffect(() => {
    if (cameraRef.current) {
      cameraRef.current.style.height = height + 'px';
    }
  }, [height]);

  React.useEffect(() => {
    setMaxVideoLength(props && props.maxVideoLength);
  }, [props]);

  const timeCount = () => {
    setRecordingLength((prev) => {
      let secs = prev.s + 1;
      let hours = Math.floor(secs / (60 * 60));

      let divisor_for_minutes = secs % (60 * 60);
      let minutes = Math.floor(divisor_for_minutes / 60);

      let divisor_for_seconds = divisor_for_minutes % 60;
      let seconds = Math.ceil(divisor_for_seconds);
      return {
        h: hours,
        m: minutes,
        s: seconds
      };
    });
  };

  const startTimer = React.useCallback(() => {
    timeRef.current = setInterval(timeCount, 1000);
  }, []);

  const handleDataAvailable = ({ data }) => {
    if (data.size > 0) {
      setRecordedChunks((prev) => prev.concat(data));
    }
  };

  const startCountdownFn = () => {
    setStartCountdown(true);
    countdownRef.current = setInterval(() => {
      setCountdown((prevCountdown) => prevCountdown - 1);
    }, 1000);
  };

  const decreaseMaxVideoLength = () => {
    maxVideoLengthRef.current = setInterval(() => {
      setMaxVideoLength((prevVidLength) => prevVidLength - 1);
    }, 1000);
  };

  React.useEffect(() => {
    if (recordingScreen) {
      try {
        if (window.MediaRecorder) {
          navigator.mediaDevices
            .getUserMedia({ audio: true, video: true })
            .then((res) => {
              if (res.active === true) {
                const stream = res;
                webcamRef.current.srcObject = stream;
                setTracks(stream.getTracks());
                mediaRecorderRef.current = new MediaRecorder(stream, {
                  mimeType: 'video/webm'
                });
                mediaRecorderRef.current.addEventListener('dataavailable', handleDataAvailable);
              } else {
                props.setMessage('Media device is not active');
              }
            })
            .catch((err) => {
              props.setMessage(
                `Error in User Media, Please check the permission again, 
                  ${err.toString()}`
              );
            });
        } else {
          let MediaRecorder = AudioRecorder;
          navigator.mediaDevices
            .getUserMedia({ audio: true, video: true })
            .then((res) => {
              if (res.active === true) {
                const stream = res;
                webcamRef.current.srcObject = stream;
                setTracks(stream.getTracks());
                mediaRecorderRef.current = new MediaRecorder(stream, {
                  mimeType: 'video/webm'
                });
                mediaRecorderRef.current.addEventListener('dataavailable', handleDataAvailable);
              } else {
                props.setMessage('Media device is not active');
              }
            })
            .catch((err) => {
              props.setMessage(
                `Error in User Media, Please check the permission again, 
                  ${err.toString()}`
              );
            });
        }
      } catch (err) {
        console.log(err);
        props.setMessage(`Error in Media device, ${err.toString()}`);
      }
    }
    // eslint-disable-next-line
  }, [recordingScreen]);

  const startRecording = React.useCallback(() => {
    setStartCountdown(false);
    setCountdown(0);
    setCapturing(true);
    startTimer();
    decreaseMaxVideoLength();
    mediaRecorderRef.current.start(10);
  }, [startTimer]);

  React.useEffect(() => {
    if (countdown === 0) {
      clearInterval(countdownRef.current);
      startRecording();
    }
  }, [countdown, startRecording]);

  const stopRecording = () => {
    mediaRecorderRef.current.stop();
    webcamRef.current.srcObject = null;
    clearInterval(timeRef.current);
    clearInterval(maxVideoLengthRef.current);
    setCapturing(false);
    const blob = new Blob(recordedChunks, {
      type: 'video/mp4'
    });
    const url = URL.createObjectURL(blob);
    setBlob(blob);
    setUrl(url);
    setRecordingScreen(false);
    webcamRef.current.src = url;
    setRecordingLength({
      h: 0,
      m: 0,
      s: 0
    });
  };

  const resetRecorder = () => {
    setRecordedChunks([]);
    setUrl(null);
    setBlob(null);
    setRecordingScreen(true);
    setCountdown(3);
    setMaxVideoLength(props && props.maxVideoLength);
  };

  React.useEffect(() => {
    if (maxVideoLength === 0) {
      stopRecording();
    }
    // eslint-disable-next-line
  }, [maxVideoLength]);

  const submitVideo = () => {
    if (recordedChunks.length) {
      blob.name = generateNameFromUrl('WebcamVideo', 'mp4');
      blob.url = url;

      console.log(blob);

      let formData = new FormData();
      formData.append('file', blob, blob.name);

      setProgressbar(true);
      axios
        .post('https://api.miljulapp.com/api/v1/common/fileUpload', formData)
        .then((response) => {
          console.log(response);
          if (response.status === 200) {
            props.setMediaUrl(response.data.url);
            setProgressbar(false);
            if (!props.createVideo) {
              if (props.collaboratorId) {
                stopTracks();
                props.createColabMedia(response.data.url, props.eventId, props.collaboratorId);
              } else {
                stopTracks();
                props.setOpenVideoCamera(false);
                props.setOptions(true);
                props.setOpenForm(true);
              }
            }
          } else {
            setProgressbar(false);
            props.setMessage('Error in File Upload Process');
          }
        })
        .catch((error) => {
          console.log(error);
          setProgressbar(false);
          props.setMessage('Error in File Upload Process');
        });

      window.URL.revokeObjectURL(url);
      setRecordedChunks([]);
    }
  };

  const stopTracks = () => {
    tracks &&
      tracks.forEach(function (track) {
        track.stop();
      });
    return;
  };

  // console.log(props);

  return (
    <div ref={cameraRef}>
      {cameraRef && cameraRef.current ? (
        <>
          {/********************************************/
          /**************** Camera Area ****************/
          /********************************************/}
          <video
            autoPlay={true}
            playsInline={true}
            ref={webcamRef}
            controls={!recordingScreen}
            muted={recordingScreen}
            style={{
              position: 'relative',
              width: '100%',
              minHeight: '100vh',
              objectFit: 'cover',
              transform: recordingScreen ? 'scaleX(-1)' : null
            }}
          ></video>

          {/************************ Close Button *******************************/}
          {!props.createVideo ? (
            <div style={{ position: 'absolute', top: 0, right: 0 }}>
              <button
                style={{
                  border: `2px solid ${props && props.theme && props.theme.themeColor}`,
                  backgroundColor: props && props.theme && props.theme.themeColor,
                  color: '#fff',
                  padding: '5px 10px',
                  borderBottomLeftRadius: '5px'
                }}
                onClick={() => {
                  stopTracks();
                  props.setOpenVideoCamera(false);
                  props.setLeftVideo(true);
                  props.setOptions(true);
                }}
              >
                <CloseIcon />
              </button>
            </div>
          ) : null}

          {recordingScreen ? (
            <>
              {/************************** Recording Length *****************************/}
              <div style={{ position: 'absolute', bottom: '15vh', left: '0', width: '100%', textAlign: 'center' }}>
                {maxVideoLength ? (
                  maxVideoLength === 0 ? (
                    <span>00: 00</span>
                  ) : (
                    <div
                      style={{
                        fontSize: '2rem',
                        textAlign: 'center',
                        color: props && props.theme && props.theme.themeColor
                      }}
                    >
                      <span>{formatTime(maxVideoLength)}</span>
                    </div>
                  )
                ) : (
                  <div
                    style={{
                      fontSize: '2rem',
                      textAlign: 'center',
                      color: props && props.theme && props.theme.themeColor
                    }}
                  >
                    <span>
                      {recordingLength.m !== undefined
                        ? `${recordingLength.m <= 9 ? '0' + recordingLength.m : recordingLength.m}`
                        : '00'}
                    </span>
                    <span>:</span>
                    <span>
                      {recordingLength.s !== undefined
                        ? `${recordingLength.s <= 9 ? '0' + recordingLength.s : recordingLength.s}`
                        : '00'}
                    </span>
                  </div>
                )}

                {/***************************** Play/Stop Button ***********************/}
                {capturing ? (
                  <button className={classes.videoButtons} onClick={stopRecording}>
                    <StopIcon
                      style={{
                        fontSize: '60px'
                      }}
                    />
                  </button>
                ) : (
                  <button className={classes.videoButtons} onClick={startCountdownFn}>
                    <FiberManualRecordIcon
                      style={{
                        fontSize: '60px'
                      }}
                    />
                  </button>
                )}
              </div>

              {/************************* Countdown *****************************/}
              {startCountdown ? (
                <div style={{ position: 'absolute', top: '0', left: '0', width: '100%', height: '100%' }}>
                  <div
                    style={{
                      display: 'flex',
                      height: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: 'rgba(0, 0, 0, 0.6)'
                    }}
                  >
                    <div style={{ fontSize: '8rem', color: props && props.theme && props.theme.themeColor }}>
                      {countdown === 0 ? 'GO' : countdown}
                    </div>
                  </div>
                </div>
              ) : null}
            </>
          ) : (
            <>
              {/********************************************/
              /*************** Preview Area ***************/
              /********************************************/}
              <div style={{ position: 'absolute', bottom: '15vh', left: '0', width: '100%', textAlign: 'center' }}>
                <button className={classes.videoButtons} style={{ marginRight: '10px' }} onClick={resetRecorder}>
                  <ReplayIcon
                    style={{
                      fontSize: '60px'
                    }}
                  />
                </button>
                <button className={classes.videoButtons} style={{ marginLeft: '10px' }} onClick={submitVideo}>
                  <DoneIcon
                    style={{
                      fontSize: '60px'
                    }}
                  />
                </button>
              </div>
              {progressbar ? <Progressbar theme={props && props.theme} /> : null}
            </>
          )}
        </>
      ) : (
        <Progressbar theme={props && props.theme} />
      )}
    </div>
  );
};

export default Camera;
