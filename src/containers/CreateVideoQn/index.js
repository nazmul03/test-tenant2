import React from 'react';
import { withRouter } from 'react-router-dom';
import { isMobile } from 'react-device-detect';

import Camera from '../Camera';
import CustomModal from '../CustomModal';

const CreateVideoQn = (props) => {
  const [message, setMessage] = React.useState(null);
  const [url, setUrl] = React.useState(null);

  React.useEffect(() => {}, [props]);

  console.log(props, message);

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <div
        className='row'
        style={{
          width: '100%',
          height: '100vh',
          boxShadow: '2px 2px 10px 10px #eee',
          backgroundColor: '#FFF',
          overflow: 'hidden',
          position: 'relative'
        }}
      >
        <div className='col-12 col-sm-6 p-0 m-0'>
          <Camera createVideo setMessage={setMessage} setMediaUrl={setUrl} theme={props && props.theme} />
        </div>
        <div className='col-12 col-sm-6 p-0 m-0' style={{ backgroundColor: '#F2F2F2' }}>
          <div
            style={{
              height: '100vh',
              width: '100%',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              padding: '1rem'
            }}
          >
            {url ? url : 'You will get an URL here after creating a video'}
          </div>
        </div>
      </div>

      {isMobile ? (
        <CustomModal open={url} onClose={() => setUrl(null)} theme={props && props.theme}>
          <div style={{ width: '80vw' }}>{url}</div>
        </CustomModal>
      ) : null}
    </div>
  );
};

export default withRouter(CreateVideoQn);
