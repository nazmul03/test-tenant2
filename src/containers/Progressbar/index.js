import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'absolute',
    top: '0',
    left: '0',
    height: '100%',
    width: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
  },
  progressArea: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    '& .MuiCircularProgress-colorPrimary': {
      color: (theme) => theme.themeColor
    }
  }
}));

export default function Progressbar(props) {
  const classes = useStyles(props && props.theme);

  return (
    <div className={classes.root}>
      <div className={classes.progressArea}>
        <CircularProgress style={{ outline: 'none', height: '180px', width: '180px' }} />
      </div>
    </div>
  );
}
