# Script for granting git access to the CI of the current repository on Bitbucket
# This script expect the enviroment variables $OAUTH_KEY and $OAUTH_SECRET, to be pressent in the bitbucket

KEY=$BB_USER
SECRET=$BB_PASSWORD

set -e

wget -q -O jq https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64
chmod 755 jq
TOKEN=`curl -s -S -f -X POST -u "${KEY}:${SECRET}" https://bitbucket.org/site/oauth2/access_token -d grant_type=client_credentials | ./jq .access_token -r`

git remote set-url origin https://x-token-auth:${TOKEN}@bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}.git