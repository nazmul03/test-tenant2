#!/usr/bin/env bash

# Exit immediately if a any command exits with a non-zero status
# e.g. pull-request merge fails because of conflict

# Call this after creating a pull request

echo "PR ID Obtained in the shell: $PR_ID"

if [[ -z "${PR_ID}" ]]; then
  echo "Pull request id not defined"
else
  # Merge PR
  echo "Merging PR: $PR_ID"
  curl -X POST https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER/$BITBUCKET_REPO_SLUG/pullrequests/$PR_ID/merge \
    --fail --show-error --silent \
    --user $BB_USER:$BB_PASSWORD \
    -H 'content-type: application/json' \
    -d '{
      "close_source_branch": false,
      "merge_strategy": "merge_commit"
    }'
fi
